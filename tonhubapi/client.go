package tonhubapi

import (
	"fmt"
	"gitlab.com/golib4/http-client/http"
)

type PriceData struct {
	Price struct {
		USD   float64 `json:"usd"`
		Rates struct {
			RUB float64
			EUR float64
		} `json:"rates"`
	} `json:"price"`
}

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	return Client{
		httpClient: httpClient,
	}
}

type PriceRequest struct {
	Date string `schema:"date"`
}

func (c Client) GetPrice(request *PriceRequest) (*PriceData, error) {
	var response PriceData
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{Path: fmt.Sprintf("/price?date=%s", request.Date)},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing tonhubapi request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing tonhubapi request: %s", httpError)
	}

	return &response, nil
}

type GetAddressBalanceResponse struct {
	Result string `json:"result"`
}

type GetAddressBalanceRequest struct {
	Address string
}

func (c Client) GetAddressBalance(request GetAddressBalanceRequest) (*GetAddressBalanceResponse, error) {
	var response GetAddressBalanceResponse
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{Path: fmt.Sprintf("/getAddressBalance?address=%s", request.Address)},
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing toncenter request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing toncenter request: %s", httpError)
	}

	return &response, nil

}
